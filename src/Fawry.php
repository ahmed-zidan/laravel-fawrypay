<?php
/**
 * Created by Ahmed Zidan.
 * email: php.ahmedzidan@gmail.com
 * Project: antikah_laravel
 * Date: 10/9/19
 * Time: 10:26 PM
 */

namespace Zidan\FawryPay;


use GuzzleHttp\Client;

class Fawry
{
    public $merchantCode;
    public $securityKey;
    protected $client;

    public function __construct()
    {
        $this->merchantCode = config('laravel-fawry.merchant.code');
        $this->securityKey = config('laravel-fawry.merchant.security_key');
        $this->client = new Client();
    }

    public function endpoint($uri)
    {
        return config('laravel-fawry.live_mode') ?
            'https://atfawry.fawrystaging.com/ECommerceWeb/Fawry/' . $uri :
            'https://www.atfawry.com/ECommerceWeb/Fawry/' . $uri;
    }

    public function createCardToken($cardNumber, $expiryYear, $expiryMonth, $cvv, $user)
    {

        $response = $this->request('post',
            $this->endpoint("cards/cardToken"), [
                "merchantCode"      => $this->merchantCode,
                "customerProfileId" => md5($user->id),
                "customerMobile"    => $user->mobile,
                "customerEmail"     => $user->email,
                "cardNumber"        => $cardNumber,
                "expiryYear"        => $expiryYear,
                "expiryMonth"       => $expiryMonth,
                "cvv"               => $cvv
            ]
        );


        if ($response->statusCode == 200) {
            return [
                'last_four_digits' => $response->card->lastFourDigits,
                'brand'            => $response->card->brand,
                'token'            => $response->card->token,
            ];
        }
        throw new \Exception(['error' => "Can't create a card token", 'response' => $response]);
    }

    public function listCustomerTokens($customer_id)
    {
        return $this->request('get',
            $this->endpoint("cards/cardToken"), [
                'merchantCode'      => $this->merchantCode,
                'customerProfileId' => md5($customer_id),
                'signature'         => hash('sha256', $this->merchantCode . md5($customer_id) . $this->securityKey),
            ]
        );
    }

    public function deleteCardToken($user)
    {
        $result = $this->request('delete',
            $this->endpoint("cards/cardToken"), [
                'merchantCode'      => $this->merchantCode,
                'customerProfileId' => md5($user->id),
                'signature'         => hash(
                    'sha256',
                    $this->merchantCode .
                    md5($user->id) .
                    $user->payment_card_fawry_token .
                    $this->securityKey
                )
            ]
        );

        if ($result->statusCode !== 200) {
            throw new \Exception($result->error, $result->statusCode);
        }

        return true;
    }

    public function cardCharge($merchantRefNum, $user, $amount, $chargeItems = [], $description = null)
    {
        return $this->request('post',
            $this->endpoint("cards/cardToken"), [
                'merchantCode'      => $this->merchantCode,
                'merchantRefNum'    => $merchantRefNum,
                'paymentMethod'     => 'CARD',
                'cardToken'         => $user->payment_card_fawry_token,
                'customerProfileId' => md5($user->id),
                'customerMobile'    => $user->mobile,
                'customerEmail'     => $user->email,
                'amount'            => $amount,
                'currencyCode'      => 'EGP',
                'chargeItems'       => $chargeItems,
                'description'       => $description,
                'signature'         => hash(
                    'sha256',
                    $this->merchantCode .
                    $merchantRefNum .
                    md5($user->id) .
                    'CARD' .
                    number_format((float)$amount, 2) .
                    $user->payment_card_fawry_token .
                    $this->securityKey
                )
            ]
        );
    }

    public function fawryCharge($merchantRefNum, $user, $paymentExpiry, $amount, $chargeItems = [], $description = null)
    {
        return $this->request('post',
            $this->endpoint("payments/charge"), [
                [
                    'merchantCode'      => $this->merchantCode,
                    'merchantRefNum'    => $merchantRefNum,
                    'paymentMethod'     => 'PAYATFAWRY',
                    'paymentExpiry'     => $paymentExpiry,
                    'customerProfileId' => md5($user->id),
                    'customerMobile'    => $user->mobile,
                    'customerEmail'     => $user->email,
                    'amount'            => $amount,
                    'currencyCode'      => 'EGP',
                    'chargeItems'       => $chargeItems,
                    'description'       => $description,
                    'signature'         => hash(
                        'sha256',
                        $this->merchantCode .
                        $merchantRefNum .
                        md5($user->id) .
                        'PAYATFAWRY' .
                        number_format((float)$amount, 2) .
                        $this->securityKey
                    )
                ]
            ]
        );
    }

    public function refund($fawryRefNumber, $refundAmount, $reason = null)
    {
        return $this->request("post",
            $this->endpoint("payments/refund"),
            [
                'merchantCode'    => $this->merchantCode,
                'referenceNumber' => $fawryRefNumber,
                'refundAmount'    => $refundAmount,
                'reason'          => $reason,
                'signature'       => hash(
                    'sha256',
                    $this->merchantCode .
                    $fawryRefNumber .
                    number_format((float)$refundAmount, 2) .
                    $this->securityKey
                )
            ]
        );
    }


    protected function request($method, $url, $params = [])
    {
        $response = $this->client->request($method, $url, ['body' => $params]);
        return $response->getBody();
    }
}
