<?php
/**
 * Created by Ahmed Zidan.
 * email: php.ahmedzidan@gmail.com
 * Project: antikah_laravel
 * Date: 10/9/19
 * Time: 11:04 PM
 */
return [
    'live_mode' => env('FAWRY_LIVE', false),
    'merchant'  => [
        'code'         => env('FAWRY_MERCHANT_CODE'),
        'security_key' => env('FAWRY_SECURITY_KEY'),
    ]
];
