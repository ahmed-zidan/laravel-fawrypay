<?php

namespace Zidan\FawryPay;

use Illuminate\Support\ServiceProvider;
use Zidan\FawryPay\Facades\FawryPayFacade;

class FawryPayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Fawry', function () {
            return new FawryPayFacade();
        });

        $this->mergeConfigFrom(
            __DIR__ . '/config/laravel-fawrypay.php', 'laravel-fawrypay'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->registerPublishing();
    }

    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/laravel-fawrypay.php' => $this->app->configPath('laravel-fawrypay.php'),
            ], 'config');
        }
    }
}
