<?php
/**
 * Created by Ahmed Zidan.
 * email: php.ahmedzidan@gmail.com
 * Project: antikah_laravel
 * Date: 10/9/19
 * Time: 10:34 PM
 */

namespace Zidan\FawryPay\Facades;

use Illuminate\Support\Facades\Facade;

class FawryPayFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Fawry';
    }
}
